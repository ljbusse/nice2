function LJBtesting(hObject, handles)
    handles = guidata(hObject);  %load handles
    buttontype=get(handles.figure1,'SelectionType');
    %get initial cursor location
    cp = get(handles.axes1,'CurrentPoint');
    handles.xinit = cp(1,1);
    handles.yinit = cp(1,2);
    %create holder variables
    handles.movingrois=false(handles.subrow,handles.subcol);
    handles.current_col=[];
    handles.current_row=[];
    handles.buttonmove=0; %initially set to no motion
    handles.newrect=cell(size(handles.subIrect));
    %determine if mouse click was inside an ROI
    for scol=1:handles.subcol
        for srow=1:handles.subrow
            if(cp(1,1)>=handles.subIrect{srow,scol}(1) && cp(1,1)<=handles.subIrect{srow,scol}(1)+handles.subIrect{srow,scol}(3) && cp(1,2)>=handles.subIrect{srow,scol}(2) && cp(1,2)<=handles.subIrect{srow,scol}(2)+handles.subIrect{srow,scol}(4))
                handles.current_col=scol;
                handles.current_row=srow;
            end
        end
    end
    %change the dragging function based on the type of mouse click
    %regular click drags all ROIs
    %right or contol click to drag columns of ROIs
    %middle or shift click to drag rows of ROIs
    switch buttontype;
    case {'normal'} % single left click, operate on a single roi
    grabbed_edge=false; %grabbed_edge refers to resizing handles
    %check the resizing handles to see if they have been selected
    for cornnum=1:4
        if(strcmp(get(handles.corner(cornnum),'UserData'),'on'))
            grabbed_edge=true;
            handles.activeCorner=cornnum; %get the active corner
            set(gcf,'WindowButtonMotionFcn',@resize_roimotion) %change the function used when mouse moves
            set(gcf,'WindowButtonUpFcn',@resize_roiup) %change what happens when button released
        end
    end
    if(~grabbed_edge); %no ROI corner is selected
        %make holder variables
        handles.current_col=[];
        handles.current_row=[];
        handles.newrect=cell(size(handles.subIrect));
        handles.movingrois=false(handles.subrow,handles.subcol);
        handles.buttonmove=0; %check if button motion happened
        %determine if button press occurred inside an ROI
        for scol=1:handles.subcol
            for srow=1:handles.subrow
                if(cp(1,1)>=handles.subIrect{srow,scol}(1) && cp(1,1)<=handles.subIrect{srow,scol}(1)+handles.subIrect{srow,scol}(3) && cp(1,2)>=handles.subIrect{srow,scol}(2) && cp(1,2)<=handles.subIrect{srow,scol}(2)+handles.subIrect{srow,scol}(4))
                    handles.current_col=scol;
                    handles.current_row=srow;
                    handles.activeROI=[srow scol]; %get the active ROI
                end
            end
        end
        handles.movingrois(handles.current_row,handles.current_col)=1; %set the active ROI
        if(max(max(handles.movingrois))~=0) %was the click inside a ROI? but not on corner
            switch buttontype;
                case {'normal','open'} % single, or double click
                    %color the active ROI red
                    set(handles.recaxis{handles.current_row,handles.current_col},'EdgeColor','r', 'LineStyle', '-');
                    set(hObject,'pointer','fleur') %fluer is 4-point arrow
                    set(hObject,'WindowButtonMotionFcn',@dragbuttonmotion) %change the function used when mouse moves
                    set(hObject,'WindowButtonUpFcn',@dragbuttonup) %change what happens when button released
            end
        else
            set(hObject,'Pointer','arrow')
            set(hObject,'WindowButtonMotionFcn','')
            set(hObject,'WindowButtonUpFcn','')
            handles.newrect=handles.subIrect;
            [handles]=reset_rectangles(hObject, handles); %replot ROIs
        end
    end
    case 'extend' % User click+shift = select row
        handles.movingrois(handles.current_row,:)=1; %set row of ROIs as actively moving
        if(~isempty(handles.current_col)) %was the click inside a ROI?
            for scol=1:handles.subcol
                for srow=handles.current_row
                    %color moving ROIs red
                    [handles]=drawROI(hObject, handles, [srow scol], 'change', 'r', []);
                end
            end
            set(hObject,'pointer','fleur') %fluer is 4-point arrow
            set(hObject,'WindowButtonMotionFcn',@dragbuttonmotion) %change the function used when mouse moves
            set(hObject,'WindowButtonUpFcn',@dragbuttonup) %change what happens when button released
        else
          set(hObject,'Pointer','arrow')
          set(hObject,'WindowButtonMotionFcn','')
          set(hObject,'WindowButtonUpFcn','')
          handles.newrect=handles.subIrect;
          [handles]=reset_rectangles(hObject, handles); %replot ROIs
        end
    case 'alt' % User click+ctrl = select column
        handles.movingrois(:,handles.current_col)=1; %set column of ROIs as actively moving
        if(~isempty(handles.current_col)) %was the click inside a ROI?
            for scol=handles.current_col
                for srow=1:handles.subrow
                    %color moving ROIs red
                    [handles]=drawROI(hObject, handles, [srow scol], 'change', 'r', []);
                end
            end
            set(hObject,'pointer','fleur') %fluer is 4-point arrow
            set(hObject,'WindowButtonMotionFcn',@dragbuttonmotion) %change the function used when mouse moves
            set(hObject,'WindowButtonUpFcn',@dragbuttonup) %change what happens when button released
        else
          set(hObject,'Pointer','arrow')
          set(hObject,'WindowButtonMotionFcn','')
          set(hObject,'WindowButtonUpFcn','')
          handles.newrect=handles.subIrect;
          [handles]=reset_rectangles(hObject, handles); %replot ROIs
        end
    end
    guidata(hObject,handles);       